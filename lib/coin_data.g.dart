// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coin_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CoinData _$$_CoinDataFromJson(Map<String, dynamic> json) => _$_CoinData(
      asset_id_base: json['asset_id_base'] as String?,
      rates: (json['rates'] as List<dynamic>?)
          ?.map((e) => Rates.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_CoinDataToJson(_$_CoinData instance) =>
    <String, dynamic>{
      'asset_id_base': instance.asset_id_base,
      'rates': instance.rates,
    };
