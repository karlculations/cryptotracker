# NOTE:

* Before this flutter project can be executed, the command below may need to be executed, inorder to build the Freeze files. Refer to Flutter's docs for further assistance if needed.

`flutter pub run build_runner build --delete-conflicting-outputs`