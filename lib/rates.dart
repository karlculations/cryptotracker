import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'rates.freezed.dart';
part 'rates.g.dart';

@freezed
class Rates with _$Rates {
  const factory Rates(String? time, String? assest_id_quote, double? rate) =
      _Rates;

  factory Rates.fromJson(Map<String, dynamic> json) => _$RatesFromJson(json);
}
