// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rates.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Rates _$$_RatesFromJson(Map<String, dynamic> json) => _$_Rates(
      json['time'] as String?,
      json['assest_id_quote'] as String?,
      (json['rate'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_RatesToJson(_$_Rates instance) => <String, dynamic>{
      'time': instance.time,
      'assest_id_quote': instance.assest_id_quote,
      'rate': instance.rate,
    };
