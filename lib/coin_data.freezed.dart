// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'coin_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CoinData _$CoinDataFromJson(Map<String, dynamic> json) {
  return _CoinData.fromJson(json);
}

/// @nodoc
class _$CoinDataTearOff {
  const _$CoinDataTearOff();

  _CoinData call({String? asset_id_base, List<Rates>? rates}) {
    return _CoinData(
      asset_id_base: asset_id_base,
      rates: rates,
    );
  }

  CoinData fromJson(Map<String, Object?> json) {
    return CoinData.fromJson(json);
  }
}

/// @nodoc
const $CoinData = _$CoinDataTearOff();

/// @nodoc
mixin _$CoinData {
  String? get asset_id_base => throw _privateConstructorUsedError;
  List<Rates>? get rates => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CoinDataCopyWith<CoinData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CoinDataCopyWith<$Res> {
  factory $CoinDataCopyWith(CoinData value, $Res Function(CoinData) then) =
      _$CoinDataCopyWithImpl<$Res>;
  $Res call({String? asset_id_base, List<Rates>? rates});
}

/// @nodoc
class _$CoinDataCopyWithImpl<$Res> implements $CoinDataCopyWith<$Res> {
  _$CoinDataCopyWithImpl(this._value, this._then);

  final CoinData _value;
  // ignore: unused_field
  final $Res Function(CoinData) _then;

  @override
  $Res call({
    Object? asset_id_base = freezed,
    Object? rates = freezed,
  }) {
    return _then(_value.copyWith(
      asset_id_base: asset_id_base == freezed
          ? _value.asset_id_base
          : asset_id_base // ignore: cast_nullable_to_non_nullable
              as String?,
      rates: rates == freezed
          ? _value.rates
          : rates // ignore: cast_nullable_to_non_nullable
              as List<Rates>?,
    ));
  }
}

/// @nodoc
abstract class _$CoinDataCopyWith<$Res> implements $CoinDataCopyWith<$Res> {
  factory _$CoinDataCopyWith(_CoinData value, $Res Function(_CoinData) then) =
      __$CoinDataCopyWithImpl<$Res>;
  @override
  $Res call({String? asset_id_base, List<Rates>? rates});
}

/// @nodoc
class __$CoinDataCopyWithImpl<$Res> extends _$CoinDataCopyWithImpl<$Res>
    implements _$CoinDataCopyWith<$Res> {
  __$CoinDataCopyWithImpl(_CoinData _value, $Res Function(_CoinData) _then)
      : super(_value, (v) => _then(v as _CoinData));

  @override
  _CoinData get _value => super._value as _CoinData;

  @override
  $Res call({
    Object? asset_id_base = freezed,
    Object? rates = freezed,
  }) {
    return _then(_CoinData(
      asset_id_base: asset_id_base == freezed
          ? _value.asset_id_base
          : asset_id_base // ignore: cast_nullable_to_non_nullable
              as String?,
      rates: rates == freezed
          ? _value.rates
          : rates // ignore: cast_nullable_to_non_nullable
              as List<Rates>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CoinData extends _CoinData with DiagnosticableTreeMixin {
  _$_CoinData({this.asset_id_base, this.rates}) : super._();

  factory _$_CoinData.fromJson(Map<String, dynamic> json) =>
      _$$_CoinDataFromJson(json);

  @override
  final String? asset_id_base;
  @override
  final List<Rates>? rates;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CoinData(asset_id_base: $asset_id_base, rates: $rates)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CoinData'))
      ..add(DiagnosticsProperty('asset_id_base', asset_id_base))
      ..add(DiagnosticsProperty('rates', rates));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CoinData &&
            const DeepCollectionEquality()
                .equals(other.asset_id_base, asset_id_base) &&
            const DeepCollectionEquality().equals(other.rates, rates));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(asset_id_base),
      const DeepCollectionEquality().hash(rates));

  @JsonKey(ignore: true)
  @override
  _$CoinDataCopyWith<_CoinData> get copyWith =>
      __$CoinDataCopyWithImpl<_CoinData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CoinDataToJson(this);
  }
}

abstract class _CoinData extends CoinData {
  factory _CoinData({String? asset_id_base, List<Rates>? rates}) = _$_CoinData;
  _CoinData._() : super._();

  factory _CoinData.fromJson(Map<String, dynamic> json) = _$_CoinData.fromJson;

  @override
  String? get asset_id_base;
  @override
  List<Rates>? get rates;
  @override
  @JsonKey(ignore: true)
  _$CoinDataCopyWith<_CoinData> get copyWith =>
      throw _privateConstructorUsedError;
}
