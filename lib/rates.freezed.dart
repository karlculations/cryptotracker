// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'rates.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Rates _$RatesFromJson(Map<String, dynamic> json) {
  return _Rates.fromJson(json);
}

/// @nodoc
class _$RatesTearOff {
  const _$RatesTearOff();

  _Rates call(String? time, String? assest_id_quote, double? rate) {
    return _Rates(
      time,
      assest_id_quote,
      rate,
    );
  }

  Rates fromJson(Map<String, Object?> json) {
    return Rates.fromJson(json);
  }
}

/// @nodoc
const $Rates = _$RatesTearOff();

/// @nodoc
mixin _$Rates {
  String? get time => throw _privateConstructorUsedError;
  String? get assest_id_quote => throw _privateConstructorUsedError;
  double? get rate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RatesCopyWith<Rates> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RatesCopyWith<$Res> {
  factory $RatesCopyWith(Rates value, $Res Function(Rates) then) =
      _$RatesCopyWithImpl<$Res>;
  $Res call({String? time, String? assest_id_quote, double? rate});
}

/// @nodoc
class _$RatesCopyWithImpl<$Res> implements $RatesCopyWith<$Res> {
  _$RatesCopyWithImpl(this._value, this._then);

  final Rates _value;
  // ignore: unused_field
  final $Res Function(Rates) _then;

  @override
  $Res call({
    Object? time = freezed,
    Object? assest_id_quote = freezed,
    Object? rate = freezed,
  }) {
    return _then(_value.copyWith(
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String?,
      assest_id_quote: assest_id_quote == freezed
          ? _value.assest_id_quote
          : assest_id_quote // ignore: cast_nullable_to_non_nullable
              as String?,
      rate: rate == freezed
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
abstract class _$RatesCopyWith<$Res> implements $RatesCopyWith<$Res> {
  factory _$RatesCopyWith(_Rates value, $Res Function(_Rates) then) =
      __$RatesCopyWithImpl<$Res>;
  @override
  $Res call({String? time, String? assest_id_quote, double? rate});
}

/// @nodoc
class __$RatesCopyWithImpl<$Res> extends _$RatesCopyWithImpl<$Res>
    implements _$RatesCopyWith<$Res> {
  __$RatesCopyWithImpl(_Rates _value, $Res Function(_Rates) _then)
      : super(_value, (v) => _then(v as _Rates));

  @override
  _Rates get _value => super._value as _Rates;

  @override
  $Res call({
    Object? time = freezed,
    Object? assest_id_quote = freezed,
    Object? rate = freezed,
  }) {
    return _then(_Rates(
      time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String?,
      assest_id_quote == freezed
          ? _value.assest_id_quote
          : assest_id_quote // ignore: cast_nullable_to_non_nullable
              as String?,
      rate == freezed
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Rates with DiagnosticableTreeMixin implements _Rates {
  const _$_Rates(this.time, this.assest_id_quote, this.rate);

  factory _$_Rates.fromJson(Map<String, dynamic> json) =>
      _$$_RatesFromJson(json);

  @override
  final String? time;
  @override
  final String? assest_id_quote;
  @override
  final double? rate;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Rates(time: $time, assest_id_quote: $assest_id_quote, rate: $rate)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Rates'))
      ..add(DiagnosticsProperty('time', time))
      ..add(DiagnosticsProperty('assest_id_quote', assest_id_quote))
      ..add(DiagnosticsProperty('rate', rate));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Rates &&
            const DeepCollectionEquality().equals(other.time, time) &&
            const DeepCollectionEquality()
                .equals(other.assest_id_quote, assest_id_quote) &&
            const DeepCollectionEquality().equals(other.rate, rate));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(time),
      const DeepCollectionEquality().hash(assest_id_quote),
      const DeepCollectionEquality().hash(rate));

  @JsonKey(ignore: true)
  @override
  _$RatesCopyWith<_Rates> get copyWith =>
      __$RatesCopyWithImpl<_Rates>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RatesToJson(this);
  }
}

abstract class _Rates implements Rates {
  const factory _Rates(String? time, String? assest_id_quote, double? rate) =
      _$_Rates;

  factory _Rates.fromJson(Map<String, dynamic> json) = _$_Rates.fromJson;

  @override
  String? get time;
  @override
  String? get assest_id_quote;
  @override
  double? get rate;
  @override
  @JsonKey(ignore: true)
  _$RatesCopyWith<_Rates> get copyWith => throw _privateConstructorUsedError;
}
