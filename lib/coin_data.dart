import 'dart:convert';

import 'package:cryptotracker/rates.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart' as http;
import 'config.dart';
import 'package:flutter/foundation.dart';

part 'coin_data.freezed.dart';
part 'coin_data.g.dart';

Config _config = Config();

const coinAPIProdURL = 'https://rest.coinapi.io/v1/exchangerate';
const coinAPIDevURL = 'https://rest-sandbox.coinapi.io/v1/exchangerate';
final apiKey = _config.coinAPIKey;

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

@freezed
class CoinData with _$CoinData {
  CoinData._(); // Added constructor
  factory CoinData({String? asset_id_base, List<Rates>? rates}) = _CoinData;

  factory CoinData.fromJson(Map<String, dynamic> json) =>
      _$CoinDataFromJson(json);

  /// Coin API call to get crypto currency data
  /// This is limited to 100 calls per day due to free trial key
  /// Pass true if data needs to be inverted
  Future getCoinData(bool? prod, String? currencySymbol, bool? invert) async {
    Map<String, String> headers = {
      "X-CoinAPI-Key": _config.coinAPIKey,
      "Accept": "application/json",
      "Accept-Encoding": "deflate, gzip",
    };

    try {
      final response = (prod != null && prod)
          ? await http.get(
              Uri.parse('$coinAPIProdURL/$currencySymbol?invert=$invert'),
              headers: headers)
          : await http.get(
              Uri.parse('$coinAPIDevURL/$currencySymbol?invert=$invert'),
              headers: headers);
      if (response.statusCode == 200) {
        print(response.headers);
        print(response.body);
        return CoinData.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('${response.statusCode}, ${response.body}');
      }
    } catch (e) {
      print(e);
    }
  }
}
